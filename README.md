# jQuery Clock plugin

Display clocks with the current time.

## Screenshot
![Screenshot](https://bitbucket.org/hvassal/uvaclock/raw/a7ab229c94cf/screenshot/clock-exemple.jpg)

##Usage

### HTML

	<div id="uva-clock-container"></div>

### Javascript

	$(function() {
		$("#uva-clock-container").uvaClock();	
	});


It's all


##Note
See exemple.html and exemple.js for more exemples.