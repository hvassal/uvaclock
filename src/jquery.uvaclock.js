(function($){

	var config;
	var clocks = [];
	var intervals = [];
	var containers = [];

	var methods = {
		init: function( options ){
			config = $.extend({
				'secondsLineColor': 'rgb(113, 167, 18)',
				'secondsTextColor': 'rgb(113, 167, 18)',

				'minutesLineColor': 'rgb(34, 114, 206)',
				'minutesTextColor': 'rgb(34, 114, 206)',

				'hoursLineColor': 'rgb(219, 92, 28)',
				'hoursTextColor': 'rgb(219, 92, 28)',

				'borderLineWidth': 3,
				'borderLineRadius': 125,
				'borderLineCap': 'butt',

				'timeLineWidth': 50,
				'timeLineRadius': 100,
				'timeLineCap': 'butt',

				'textFont': 'bold 75pt Arial',

				'timerDelay': 1000,

				'clocks': { 'hours': 1  , 'minutes': 1 , 'seconds': 1 }

			}, options);
						
			var currentTime = new Date();
			var clock = new UvaClock(config);

			if( clock.HoursClockDisplayed()){
				this.append('<canvas width="275" height="275" class="uva-clock-hours uva-clock-box"></canvas>');
				clock.hoursCanvas = this.find('.uva-clock-hours')[0];				
				clock.hoursContext = clock.hoursCanvas.getContext("2d");
				clock.drawHoursClock(currentTime.getHours());
			}
			
			if( clock.MinutesClockDisplayed()){
				this.append('<canvas width="275" height="275" class="uva-clock-box uva-clock-minutes"></canvas>');
				clock.minutesCanvas = this.find('.uva-clock-minutes')[0];	
				clock.minutesContext = clock.minutesCanvas.getContext("2d");				
				clock.drawMinutesClock(currentTime.getMinutes());
			}

			if( clock.SecondsClockDisplayed()){
				this.append('<canvas width="275" height="275" class="uva-clock-box uva-clock-seconds"></canvas>');
				clock.secondsCanvas = this.find('.uva-clock-seconds')[0];					
				clock.secondsContext = clock.secondsCanvas.getContext("2d");
				clock.drawSecondsClock(currentTime.getSeconds());
			}
			
			var containerId = this.attr('id');
			intervals.push( { 
				'containerId' : containerId, 
				'timer': setInterval(function(){
							clock.clockAnimation(clock);
						 }, clock.config.timerDelay),
			});

			clocks.push({
				'containerId' : containerId, 
				'clock': clock
			});

			containers.push(this);
		},

		/*
		 * Destroy all clocks and associated elements (objects, timers, Html contents)
		 */
		destroy: function(){			
			jQuery.each(intervals, function(i, elt){				
				clearInterval(elt.timer);
			});

			intervals = [];
			clocks = [];
			jQuery.each(containers, function(i, elt){				
				elt.empty();
			});			
		},

		/*
		 * Pause all clocks
		 */
		pauseAll: function(){
			jQuery.each(intervals, function(i, elt){				
				clearInterval(elt.timer);
			});
			intervals = [];
		},

		/*
		 * Pause the current clock
		 */
		pause: function(){
			var index = findContainerIndex(this.attr('id'), intervals);			
			if( index >= 0){				
				clearInterval(intervals[index].timer);
				intervals.splice(index, 1);
			}		
		},

		/*
		 * Starts all clocks
		 */
		startAll: function(){
			intervals = [];
			jQuery.each(clocks, function(i, currentClock){
				intervals.push( { 
				'containerId' : currentClock.containerId, 
				'timer': setInterval(function(){
							currentClock.clock.clockAnimation(currentClock.clock);
						 }, currentClock.clock.config.timerDelay),
				});
			});
		},

		/*
		 * Start the current clock
		 */
		start: function(){
			// Search if the timer already exists.
			// If exists, the clock is already started,
			var index = findContainerIndex(this.attr('id'), intervals);
			var clockIndex = findContainerIndex(this.attr('id'), clocks);

			if( index == -1 && clockIndex >= 0 ){
				// Timer doesn't exists : whe should start it
				var currentClock = clocks[clockIndex];
				intervals.push( { 
				'containerId' : currentClock.containerId, 
				'timer': setInterval(function(){
							currentClock.clock.clockAnimation(currentClock.clock);
						 }, currentClock.clock.config.timerDelay),
				});
			}
		}

	}

	/*
	 * Return -1 if containerId not found
	 */
	function findContainerIndex( searchedContainerId, list){		
		if(searchedContainerId != null && searchedContainerId != ""){

			var found = false;
			var i = 0;			
			while( found == false && i < list.length){
				if( list[i].containerId == searchedContainerId){					
					found = true;
				}
				i++;
			}
			return (found == true) ? (i- 1) : -1;
		}	
	}

	/**
	 *  Plugin entry point
	 */
	$.fn.uvaClock = function(method){
	  // Method calling logic
	    if ( methods[method] ) {
	      return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
	    } 
	    else if ( typeof method === 'object' || ! method ) {
	      return methods.init.apply( this, arguments );
	    } 
	    else {
	      $.error( 'Method ' +  method + ' does not exist on jQuery.uvaClock' );
	    }   
	};	

})(jQuery);


/*
 * Clock Object definition
 */

function UvaClock(config) {
	this.config  = config;
	this.hoursCanvas;
	this.hoursContext;

	this.secondsCanvas;
	this.secondsContext;

	this.minutesCanvas;
	this.minutesContext;
}

UvaClock.prototype.HoursClockDisplayed = function(){return (this.config.clocks.hours != undefined && this.config.clocks.hours == true);}
UvaClock.prototype.MinutesClockDisplayed = function(){return (this.config.clocks.minutes != undefined && this.config.clocks.minutes == true);}
UvaClock.prototype.SecondsClockDisplayed = function(){return (this.config.clocks.seconds != undefined && this.config.clocks.seconds == true);}

UvaClock.prototype.drawHoursClock = function(value){		
	this.drawOuterClock(this.hoursContext, this.hoursCanvas, {
		'LineColor': this.config.hoursLineColor
	});
	this.moveHours(value);
}
UvaClock.prototype.drawMinutesClock = function(value){		
	this.drawOuterClock(this.minutesContext, this.minutesCanvas, {
		'LineColor': this.config.minutesLineColor
	});
	this.moveMinutes(value);
}
UvaClock.prototype.drawSecondsClock = function(value){	
	this.drawOuterClock(this.secondsContext, this.secondsCanvas, {
		'LineColor': this.config.secondsLineColor
	});
	this.moveSeconds(value);
}

UvaClock.prototype.moveHours = function(value){
	this.moveClockValue(this.hoursContext, this.hoursCanvas, value, 'hour', {
		'strokeStyle' : this.config.hoursLineColor,
		'textStyle': this.config.hoursTextColor
	});
}
UvaClock.prototype.moveMinutes = function(value){
	this.moveClockValue(this.minutesContext, this.minutesCanvas, value, 'minute', {		
		'strokeStyle' : this.config.minutesLineColor,
		'textStyle': this.config.minutesTextColor
	});
}
UvaClock.prototype.moveSeconds = function(value){
	this.moveClockValue(this.secondsContext, this.secondsCanvas, value, 'second', {		
		'strokeStyle' : this.config.secondsLineColor,
		'textStyle': this.config.secondsTextColor
	});
}


/* 
 * Used to move the clock value
 */
UvaClock.prototype.moveClockValue = function(context, canvas, value, unit, clockColorConfiguration){
	if(value == 1){					
		// 1 Clear clock content
		this.clearClock(context, canvas);		

		//2 Add clock arround
		this.drawOuterClock(context, canvas, {
			'LineColor': clockColorConfiguration.strokeStyle
		});
		//drawBorderSeconds();		
	}

	// Now, we can draw the seconds circle
	var angles = this.convertTimeToAngle(value, unit);	
	var XY = this.calculateClockXY(canvas);

	this.drawCircle(context,{
		'x': XY.x,
		'y' : XY.y,
		'radius' : this.config.timeLineRadius,
		'lineWidth': this.config.timeLineWidth,
		'lineCap': this.config.timeLineCap,
		'strokeStyle' : clockColorConfiguration.strokeStyle
	},
	angles);

	this.drawText(context, {
		'textFont': this.config.textFont, 
		'textStyle': clockColorConfiguration.textStyle,
		'x': XY.x - 57,
		'y': XY.y + 34
	}, this.pad2(value))
}

UvaClock.prototype.drawOuterClock = function(context, canvas, clockColorConfiguration){		
	var XY = this.calculateClockXY(canvas);

	this.drawCircle(context,{
		'x': XY.x,
		'y' : XY.y,
		'radius' : this.config.borderLineRadius,
		'lineWidth': this.config.borderLineWidth,
		'lineCap': this.config.borderLineCap,
		'strokeStyle' : clockColorConfiguration.LineColor
	},
	{'startAngle': Math.PI, 'endAngle': -Math.PI});	
}

UvaClock.prototype.calculateClockXY = function(canvas){
	return { 
		'x': canvas.width / 2 ,
		'y': canvas.height / 2
	};
}

UvaClock.prototype.drawCircle = function(context, configuration, angles){
	context.beginPath();
	context.arc(configuration.x, configuration.y, configuration.radius, angles.startAngle, angles.endAngle, false); 	
	context.lineWidth = configuration.lineWidth;
	context.lineCap = configuration.lineCap;
	context.strokeStyle = configuration.strokeStyle;	
	context.stroke();
}

UvaClock.prototype.drawText = function(context, configuration, text){

	this.clearText(context, configuration);
	context.font = configuration.textFont;		
	context.fillStyle = configuration.textStyle;	
	context.fillText(text, configuration.x, configuration.y);
}

/*
 *  Clear the area in which the text is drawned
 */
UvaClock.prototype.clearText = function(context, configuration){	
	context.clearRect(configuration.x, configuration.y - 80 , 110, 90);	
}

/*
 * Clear all the canvas to refresh the clock
 */
UvaClock.prototype.clearClock = function(context, canvas){	
	context.clearRect(0, 0 ,  canvas.width,  canvas.height);
}


/*
 * Function called every "config.timerDelay" to move the clocks values
 */
UvaClock.prototype.clockAnimation = function(object){
	var currentTime = new Date();

	if( object.HoursClockDisplayed()){
		object.moveHours(currentTime.getHours());	
	}
		
	if( object.MinutesClockDisplayed()){
		object.moveMinutes(currentTime.getMinutes());
	}

	if( object.SecondsClockDisplayed()){			
		object.moveSeconds(currentTime.getSeconds());
	}
}

UvaClock.prototype.pad2 = function(number) {   
     return (number < 10 ? '0' : '') + number   
}



/*
 * Convert an hour or a minute to start and end angle in 
 * order to build the clock time part
 * type: 'hour', 'minute', 'second'. Default is minute (if 'toto' is passed, it will be considered as minutes)
 */
UvaClock.prototype.convertTimeToAngle = function(value, type){
	var divider = 60;

	if( type == 'hour'){
		divider = 12; // Not 24 for common usage
		value = value % 12;
	}
	

	// Cerle divisé en "divider" éléments.
	// Le premier élement se situe de
	//  (Math.PI + Math.PI /2)  à (Math.PI / 2) + 1 * ((Math.PI * 2) / divider)

	var startAngle = (Math.PI + Math.PI /2);
	var endAngle  = (startAngle + value * ((Math.PI * 2) / divider)) -0.0001;

	return { 'startAngle' : startAngle, 'endAngle': endAngle };
}
