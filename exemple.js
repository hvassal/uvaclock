$(function() {


	$("#uva-clock-container").uvaClock();

	$("#uva-clock2-container").uvaClock({
		'secondsLineColor': 'pink',
		'secondsTextColor': 'white',

		'minutesLineColor': 'red',
		'minutesTextColor': 'white',

		'hoursLineColor': 'orange',
		'hoursTextColor': 'pink',

		'clocks': { 'hours': true  , 'minutes': true }
	});

	

	$("#destroyAll").click(function(){
		jQuery().uvaClock('destroy');
	});

	$("#pauseTimers").click(function(){
		jQuery().uvaClock('pauseAll');
	});

	$("#startTimers").click(function(){
		jQuery().uvaClock('startAll');
	});

	$("#pauseFirst").click(function(){
		$("#uva-clock-container").uvaClock('pause');
	});

	$("#startFirst").click(function(){
		$("#uva-clock-container").uvaClock('start');
	});
	
	//


});